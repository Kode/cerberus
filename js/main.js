$(document).ready(function() {
	$(".myChart").each(function( index ) {
	  //console.log( index + ": " + $( this ).text() );
		//Get context with jQuery - using jQuery's .get() method.
		
		var ctx = $(".myChart").get(index).getContext("2d");
		//This will get the first returned node in the jQuery collection.
		var myNewChart = new Chart(ctx);
		
		var data = [
			{
				value: $(this).data("used"),
				color:"#c92525"
			},
			{
				value : $(this).data("free"),
				color : "#8cc951"
			}
		
		]
		var options =
			{
				segmentStrokeColor : "#1e1e1e",
				segmentStrokeWidth : 2,
			}
		
		new Chart(ctx).Doughnut(data,options);
	});


    $(window).on("scroll", function() {
        var fromTop = $("body").scrollTop();
        $('#sticky-main-nav').toggleClass("show", (fromTop > 200));
    });
	
	
	$("div.tab_container").css("height", $(this).find("div.showscale").outerHeight()+"px");
	//$( ".tabs" ).tabs();
	$( ".tabs li a" ).on("click", function() { 
		$( ".tabs li" ).removeClass("active");
		$( this ).parent().addClass("active");
		var ind = $(".tabs li a").index(this);
		
		var allbox = $( ".tabs .addontab .inner" );
		var box = $( ".tabs .addontab .inner" ).eq(ind);
		var current = $( ".tabs .addontab .showscale" );
		var newheight = box.outerHeight();
		current.removeClass("showscale");
		current.one('transitionend', function(e) {  
			box.closest("div.tab_container").css("height", newheight+"px");
			box.addClass("showscale"); 
		});  
		return false;
	});
	
});