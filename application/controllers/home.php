<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		
		$disks = $this->config->item("unraid_disks");
		$data["disks"] = $disks;
		$ignore_disks = array("parity", "cache", "flash");
		$data["ignore_disks"] = $ignore_disks;
		//print_r($data["disks"]);
		$array_total = 0;
		$array_free = 0;
		
		$ignore_disks = array("parity", "cache", "flash");
		$a=0; // only used for demo purposes, remove when live
		$warn_count = 0;
		$warning_list = '';
		$all_list = '';
		$narray_disk_count = 0;
		$narray_list = '';
		foreach($disks as $diskname => $disk) {
			
			if(!in_array($diskname, $ignore_disks)) {
				$a++;
				
		        $array_total += $disk["fsSize"];
		        $array_free += $disk["fsFree"];
				
				$tempwarning = "33";
				$errors = $disk["numErrors"];
				if($errors > "0" || $disk["temp"] >= $tempwarning) { // add to the warning list
					$warn_count++;
					$warning_list .= $this->build_disk_line($disk, $tempwarning);
				}
				$all_list .= $this->build_disk_line($disk, $tempwarning);
			}
		} 
		
		$narray_disks = array(
			array("id" => "TOSHIBA_DT01ACA300_73H0DPHGS", "device" => "sdx", "fsSize" => "2930177100", "fsFree" => "2139029283", "temp" => "32", "mounted" => 1),
			array("id" => "TOSHIBA_DT01ACA300_73H0DPHGS", "device" => "sdy", "fsSize" => "2930177100", "fsFree" => "2139029283", "temp" => "30", "mounted" => 0),
			array("id" => "TOSHIBA_DT01ACA300_73H0DPHGS", "device" => "sdz", "fsSize" => "2930177100", "fsFree" => "2139029283", "temp" => "27", "mounted" => 0)
		);
		foreach($narray_disks as $ndisk) {
			$narray_disk_count++;
			$narray_list .= $this->build_narray_disk_line($ndisk, $tempwarning);
		}
		
		$warning_list = (!empty($warning_list)) ? $warning_list : '<p>There are currently no disks needing attention.</p>';
		$narray_list = (!empty($narray_list)) ? $narray_list : '<p>There are currently no non-array disks detected.</p>';
		
		$data["warn_count"] = $warn_count;
		$data["all_count"] = $a;
		$data["narray_disk_count"] = $narray_disk_count;
		$data["array_total"] = $array_total;
		$data["array_free"] = $array_free;

		$data["warning_list"] = $warning_list;
		$data["all_list"] = $all_list;
		$data["narray_list"] = $narray_list;

		$cpuinfo = $this->linuxstat->getCpuInfo();
		$processes = $this->linuxstat->getProcesses();
		$uptime = $this->linuxstat->getUptime();
		$memory = $this->linuxstat->getMemStat();
		$data["cpuinfo"] = $cpuinfo["model name"];
		$data["processcount"] = $processes;
		$data["uptime"] = $uptime["uptime"];
		$data["load"] = $uptime["load"];
		//print_r($memory);
		$data["memory"] = round(intval($memory["MemTotal"])/1048576).'GB <span>('.round((intval($memory["MemFree"])+intval($memory["Buffers"])+intval($memory["Cached"]))/1048576).'GB Available / '.round(intval($memory["Cached"])/1048576).'GB cached)';
		//print_r($var);
		$this->load->view('header', $data);
		$this->load->view('home', $data);
		$this->load->view('footer', $data);
	}
	
	public function build_disk_line($disk, $tempwarning) {
		$dsize = $disk["fsSize"]*1024;
		$free = $disk["fsFree"]*1024;
		$used = $dsize-$free;
		$dsize = ( $dsize > 0 ) ? $dsize : 1;
		$used = ( $used > 0 ) ? $used : 1;
		$used_size = ($used/$dsize)*100;
		$temp_warning_class = ($disk["temp"] >= $tempwarning) ? ' redtext' : '';
		
		$errors = $disk["numErrors"];
		$error_warning_class = ($errors > "0") ? ' redtext' : '';

		return '
			<div class="table-box">
				<div class="col col1">'.spin_disk($disk["name"],$disk["idx"],"url").'<div class="disk-ref">'.$disk["name"].'</div><div class="disk-name">'.$disk["id"].' ('.$disk["device"].')</div></div>
				<div class="col col2"><div class="temp'.$temp_warning_class.'">'.$disk["temp"].'<span>&deg;C</span></div></div>
				<div class="col col3"><div class="temp">'.format_bytes($dsize).'</div></div>
				<div class="col overcol1">
					<div class="col col4"><div class="colspace">'.format_bytes($used).' <span> USED</span></div></div>
					<div class="col col5"><div class="colspace">'.format_bytes($free).' <span> FREE</span></div></div>
					<div class="space-info">
						<div class="space"><div class="used" style="width: '.$used_size.'%"></div></div>
					</div>
				</div>
				<div class="col col6"><div class="temp'.$error_warning_class.'">'.$errors.' <span>Errors</span></div></div>
				<div style="clear:both;"></div>
			</div>';

	}
	
	public function build_narray_disk_line($disk, $tempwarning) {
		$dsize = $disk["fsSize"]*1024;
		$free = $disk["fsFree"]*1024;
		$used = $dsize-$free;
		$used_size = ($used/$dsize)*100;
		$temp_warning_class = ($disk["temp"] >= $tempwarning) ? ' redtext' : '';
		
		$show_used = ($disk["mounted"] === 1) ? format_bytes($used) : 0;
		$show_free = ($disk["mounted"] === 1) ? format_bytes($dsize) : 0;
		$show_used_size = ($disk["mounted"] === 1) ? $used_size : 0;
		
		return '
			<div class="table-box">
				<div class="col col1">'.mount_disk($disk).'<div class="disk-ref">'.$disk["device"].'</div><div class="disk-name">'.$disk["id"].'</div></div>
				<div class="col col2"><div class="temp'.$temp_warning_class.'">'.$disk["temp"].'<span>&deg;C</span></div></div>
				<div class="col col3"><div class="temp">'.format_bytes($dsize).'</div></div>
				<div class="col overcol1">
					<div class="col col4"><div class="colspace">'.$show_used.' <span> USED</span></div></div>
					<div class="col col5"><div class="colspace">'.$show_free.' <span> FREE</span></div></div>
					<div class="space-info">
						<div class="space"><div class="used" style="width: '.$show_used_size.'%"></div></div>
					</div>
				</div>
				<div class="col col6"></div>
				<div style="clear:both;"></div>
			</div>';

	}
	
	public function about()
	{

		$data["disks"] = $this->config->item("unraid_disks");
		//print_r($var);
		$this->load->view('header', $data);
		$this->load->view('about', $data);
		$this->load->view('footer', $data);
	}
	public function spin_disk($diskidx, $direction="down")
	{
		$cmd = "/root/mdcmd&arg1=spin{$direction}&arg2={$diskidx}";
		echo $cmd;
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */