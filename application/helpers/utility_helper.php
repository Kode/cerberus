<?php
function format_bytes($bytes, $is_drive_size=true, $beforeunit='<span>', $afterunit='</span>')
{
    $labels = array('B','KB','MB','GB','TB');
    for($x = 0; $bytes >= 1000 && $x < (count($labels) - 1); $bytes /= 1000, $x++); // use 1000 rather than 1024 to simulate HD size not real size
    if($labels[$x] == "TB") return(round($bytes, ($is_drive_size)?1:2).$beforeunit.$labels[$x].$afterunit);
    else return(round($bytes, ($is_drive_size)?0:2).$beforeunit.$labels[$x].$afterunit);
}

function isRunning($disk) {
	try {
		$CI =& get_instance();

		$pid = $CI->session->userdata('preclear_'.$disk);
		if(isset($pid) && !empty($pid)) {
			$result = shell_exec(sprintf('ps %d', $pid));
			if(count(preg_split("/\n/", $result)) > 2) {
				return true;
			} else {
				$CI->session->unset_userdata('preclear_'.$disk);
			}
		} else return false;
	} catch(Exception $e) {}

	return false;
}
function split_text($text, $len=18) {
	if(strlen($text) > $len) {
		return substr($text, 0, $len)."<br />".substr($text, $len);
	} else return $text;
}

function spin_disk($disk, $idx, $type="url") {
	$CI =& get_instance();
	$disks = $CI->config->item("unraid_disks");
	$spinning = strpos($disks[$disk]["color"],"blink")===false ? true : false;
	switch($type) {
		case "url":
			if($spinning === true) return '<img class="disk" src="/img/disk.png" alt="'.$disk.'" /><a title="Spin Down" href="/index.php/home/spin_disk/'.$idx.'/down/" class="spin"><i class="icon-shuffle"></i></a>';
			else return '<img class="disk spundown" src="/img/disk.png" alt="'.$disk.'" /><a title="Spin Up" href="/index.php/home/spin_disk/'.$idx.'/up/" class="spin"><i class="icon-loop2"></i></a>';
			break;
	}	
}
function mount_disk($disk, $type="url") {
	$CI =& get_instance();
	$mounted = $disk["mounted"]===1 ? true : false;
	switch($type) {
		case "url":
			if($mounted === true) return '<img class="disk" src="/img/disk.png" alt="Disk is mounted" /><a title="Un-Mount Disk" href="/index.php/home/unmount_disk/'.$disk["device"].'/" class="spin"><i class="icon-checkbox-checked"></i></a>';
			else return '<img class="disk spundown" src="/img/disk.png" alt="Disk is not mounted" /><a title="Mount Disk" href="/index.php/home/mount_disk/'.$disk["device"].'/" class="spin"><i class="icon-checkbox-unchecked"></i></a>';
			break;
	}	
}

?>