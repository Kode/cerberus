<?php
$var = $this->config->item("unraid_vars");
?>

        <section id="page" class="body">
            <div class="inset-box big-inset">
            	<div id="over-capacity"><i class="icon-pie"></i> Capacity</div>
            	<div id="over-free"><?php echo format_bytes($array_free*1024, true, '<span>', '');?><br />free</span></div>
            </div>
            <aside id="systemspec">
            	<ul>
                	<li><span class="greentext">SERVER</span> <?php echo $var["NAME"];?> - <?php echo $uptime;?></li>
                	<li><span class="greentext">STATUS</span> <?php echo ucwords(strtolower($var["mdState"]));?></li>
                	<li><span class="greentext">LOAD</span> <?php echo $load;?></li>
                	<li><span class="greentext">MODEL</span> <?php echo $cpuinfo;?></li>
                	<li><span class="greentext">PROCESSES</span> <?php echo $processcount; ?></li>
                    <li><span class="greentext">MEMORY</span> <?php echo $memory;?></li>
                    <li><span class="greentext">IP ADDRESS</span> <?php echo $var["IPADDR"];?></li>
            	</ul>
            </aside>
            <div class="main-buttons">
            <div class="inset-box box-info"><span class="box-title">Last Parity Check</span><p><span>73</span> days ago, duration 13hrs<br /><span>0</span> Errors Found</p></div>
            <a class="button darkgreybutton" style="margin-right:10px;" href="/"><i class="icon-lightning"></i><span class="inner-button">Stop Array</span></a>
            <a class="button greybutton" style="margin-right:10px;" href="/"><i class="icon-spinner3"></i><span class="inner-button">Check Parity</span></a>
            <a class="button greybutton" style="margin-right:10px;" href="/"><i class="icon-spinner3"></i><span class="inner-button">Spin Up All Disks</span></a>
            <a class="button greybutton" style="margin-right:10px;" href="/"><i class="icon-spinner3"></i><span class="inner-button">Spin Down All Disks</span></a>

            </div>
            <div class="hr"></div>

        </section>
        
        <section id="disks" class="body">

			<?php if(isset($disks["parity"]) && !empty($disks["parity"]) && $disks["parity"]["status"] != 'DISK_NP') { ?>
			
            <div class="inset-box disk-info">
                
                <img class="disk" src="/img/disk.png" alt="Parity" />
                <div><?php echo split_text($disks["parity"]["id"])." (".$disks["parity"]["device"].")";?></div>
                <div class="temp"><?php echo $disks["parity"]["temp"];?><span>&deg;C</span></div>
                <div class="mini-disk-info">
                    Size: <span><?php echo format_bytes(($disks["parity"]["size"]*1024));?></span><br />
                    Errors: <span>0</span>
                </div>
                <img class="ribbon" src="/img/green-ribbon.png" alt="Parity" />
                <div class="disk-ref">Parity</div>
            </div>
            <?php } else { ?>
            <div class="inset-box disk-info no-disk">
                
                <img class="disk" src="/img/disk.png" alt="No Cache" />
                <div></div>
                <div class="temp">No Parity Drive</div>
                <img class="ribbon" src="/img/green-ribbon.png" alt="Cache" />
                <div class="disk-ref">Parity</div>
            </div>

            <?php } ?>
            
            <?php if(isset($disks["cache"]) && !empty($disks["cache"]) && $disks["cache"]["status"] != 'DISK_NP') { ?>
            <div class="inset-box disk-info">
                
                <img class="disk" src="/img/disk.png" alt="Cache" />
                <div><?php echo split_text($disks["cache"]["id"])." (".$disks["cache"]["device"].")";?></div>
                <div class="temp"><?php echo $disks["cache"]["temp"];?><span>&deg;C</span></div>
                <div class="mini-disk-info">
                    Size: <span><?php echo format_bytes(($disks["cache"]["size"]*1024));?></span><br />
                    Errors: <span>0</span>
                </div>
                <img class="ribbon" src="/img/green-ribbon.png" alt="Cache" />
                <div class="disk-ref">Cache</div>
                <?php
                    $pdsize = $disks["cache"]["fsSize"]*1024;
                    $pfree = $disks["cache"]["fsFree"]*1024;
                    $pused = $pdsize-$pfree;
                    $pused_size = ($pused/$pdsize)*100;

                ?>
                <div class="space-info">
                    <?php echo format_bytes($pused, true, '', '');?> Used / <?php echo format_bytes($pfree, true, '', '');?> Free 
                    <div class="space"><div class="used" style="width: <?php echo $pused_size;?>%"></div></div>
                </div>
            </div>
            <?php } else { ?>
            <div class="inset-box disk-info no-disk">
                
                <img class="disk" src="/img/disk.png" alt="No Cache" />
                <div></div>
                <div class="temp">No Cache Drive</div>
                <img class="ribbon" src="/img/green-ribbon.png" alt="Cache" />
                <div class="disk-ref">Cache</div>
            </div>

            <?php } ?>
            <div class="inset-box disk-info">
                
                <img class="disk" src="/img/flash.png" alt="Flash" />
                <div><?php echo split_text($var["flashGUID"])." (".$disks["flash"]["device"].")";?></div>
                <div class="temp">*</div>
                <div class="mini-disk-info">
                    Size: <span><?php echo format_bytes(($disks["flash"]["size"]*1024));?></span><br />
                    Errors: <span>0</span>
                </div>
                <img class="ribbon" src="/img/green-ribbon.png" alt="Flash" />
                <div class="disk-ref">Flash</div>
                <?php
                    $fdsize = $disks["flash"]["fsSize"]*1024;
                    $ffree = $disks["flash"]["fsFree"]*1024;
                    $fused = $fdsize-$ffree;
                    $fused_size = ($fused/$fdsize)*100;

                ?>
                <div class="space-info">
                    <?php echo format_bytes($fused, true, '', '');?> Used / <?php echo format_bytes($ffree, false, '', '');?> Free 
                    <div class="space"><div class="used" style="width: <?php echo $fused_size;?>%"></div></div>
                </div>
            </div>
            
            <div class="hr"></div>
            
            
            <section id="datadisks" class="body">
                <div class="tabs">
                    <ul>
                        <li class="active"><a href="#tabs-1">Attention Required <span class="lightertext">(<?php echo $warn_count;?>)</span></a></li>
                        <li><a href="#tabs-2">All Disks <span class="lightertext">(<?php echo $all_count;?>)</span></a></li>
                        <li><a href="#tabs-3">Non-array Disks (<?php echo $narray_disk_count;?>)</a></li>
                        <li><a href="#tabs-4">Options</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="addontab" id="tabs-1">
                            <div class="inner transition hidescale showscale">
                                <!-- items needing attention -->
								<?php echo $warning_list; ?>
                            </div>
                        </div>
                        <div class="addontab" id="tabs-2">
                            <div class="inner transition hidescale">
                                <!-- all disks -->
                                <?php echo $all_list; ?>
                                
                            </div>
                        </div>
                        <div class="addontab" id="tabs-3">
                            <div class="inner transition hidescale">
                                <!-- options -->
                                <?php echo $narray_list; ?>
                            </div>
                        </div>
                        <div class="addontab" id="tabs-4">
                            <div class="inner transition hidescale">
                                <!-- options -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hr"></div>
            
            </section>
            
            

            
            <div class="hr"></div>
        </section>
