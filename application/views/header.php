<?php
$var = $this->config->item("unraid_vars");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>unRAID</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="/<?=$var['mdColor']?>.png">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/fonts.css">
        <link href='http://fonts.googleapis.com/css?family=Kaushan+Script|Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/css/main.css">
        <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		<header id="banner" class="fullwidth">
        	<section id="topbar" class="fullwidth">
            	<section id="toplinks" class="body">
                	<a id="logo" href="/">unRAID <span>v<?php echo $var["version"];?></span></a>
                    <div class="themelogo">Cerberus</div>
                    <section id="topbuttons">
                        <a class="button greenbutton" style="margin-right:10px;" href="/"><i class="icon-file"></i>Logs</a>
                        <a class="button greenbutton" href="/"><i class="icon-file"></i>Info</a>
                    </section>
                </section>
                
            </section>
            <nav id="main-nav" class="body"><ul>
                <li class="active"><a href="/"><i class="icon-home"></i><p>Main</p></a></li>
                <li><a href="/index.php/shares/"><i class="icon-tree"></i><p>Shares</p></a></li>
         
                <li><a href="/index.php/users/"><i class="icon-users"></i><p>Users</p></a></li>
                <li><a href="/index.php/settings/"><i class="icon-settings"></i><p>Settings</p></a></li>
                <li><a href="/index.php/utilities/"><i class="icon-cogs"></i><p>Utilities</p></a></li>
                <li><a href="/index.php/addons/"><i class="icon-library"></i><p>Addons</p></a></li>
                <li><a href="/index.php/home/about/"><i class="icon-info"></i><p>About</p></a></li>
            </ul></nav>
            <nav id="sticky-main-nav"><ul class="body">
                <li class="active"><a href="/"><i class="icon-home"></i><p>Main</p></a></li>
                <li><a href="/index.php/shares/"><i class="icon-tree"></i><p>Shares</p></a></li>
         
                <li><a href="/index.php/users/"><i class="icon-users"></i><p>Users</p></a></li>
                <li><a href="/index.php/settings/"><i class="icon-settings"></i><p>Settings</p></a></li>
                <li><a href="/index.php/utilities/"><i class="icon-cogs"></i><p>Utilities</p></a></li>
                <li><a href="/index.php/addons/"><i class="icon-library"></i><p>Addons</p></a></li>
                <li><a href="/index.php/home/about/"><i class="icon-info"></i><p>About</p></a></li>
            </ul></nav>
            <div class="hr"></div>
        </header>